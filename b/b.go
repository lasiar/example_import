package b

type B struct {
	A, B int
}

func (b B) Sum() int {
	return b.A + b.B
}
