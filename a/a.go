package a

import (
	"fmt"

	"test/b"
)

func PrintB(b b.B) {
	fmt.Printf("%v", b.Sum())
}
